This Application is tested on Linux Distribution! I don't support Windows :)

* To run on Linux or Mac:
    1. Open a terminal
    2. Navigate to the directory containing the files
    3. Execute the `Main.py` file by typing following command -
       `python3 Main.py`

* To run on Windows please search online


**P.S.-** Requires Python3 and PyQt5 to be installed