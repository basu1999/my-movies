#!/usr/bin/python3

#Imports
#----------
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import pickle

#custom modules
import ui_myMoviesBlack 
import ui_myMoviesWhite
import moviedata
import addeditmoviedlg
import resources

#-----------

styleSheetWhite="""
    QMainWindow{    
            background-color: white;
            color: black;}
    """

styleSheetBlack="""
    QMainWindow{
            background-color:black;}
    QTableWidget{
            background-color:black;
            color: white;}
    QMenuBar{
            background-color:black;
            color: white;}
    QStatusBar{
            background-color:black;
            color: white;}
    QToolBar{
            background-color:black;
            color: white;}
    QWidget{
            background-color:black;
            color: white;}

    """

#Class
class MyMovies(QMainWindow):
    def __init__(self):

        #GUI composition and initial setups
        super().__init__()
        self.ui= ui_myMoviesBlack.Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("My Movies")
        status=self.statusBar()
        status.setSizeGripEnabled(False)
        status.showMessage("All set...",5000)

        #loading the movies data and showing 
        self.movies=moviedata.MovieContainer()
        self.setCentralWidget(self.ui.tableWidget)
        self.ui.tableWidget.itemDoubleClicked.connect(self.editEdit)
        QShortcut(QKeySequence("Return"),self.ui.tableWidget,self.editEdit)
        self.updateTable()

        #loading previous states
        settings=QSettings()
        self.restoreGeometry(settings.value('MainWindow/Geometry', QByteArray()))
        self.restoreState(settings.value("MainWindow/State",QByteArray()))
        if settings.value("MainWindow/Theme"):
            self.theme=settings.value("MainWindow/Theme")
        else:
            self.theme="Black"
        self.toggleTheme(self.theme)
        

        QTimer.singleShot(0,self.loadInitialFiles)


                
        #---Connecting actions-----
        #File actions
        self.actionSettings(self.ui.action_New,"filenew",QKeySequence.New,self.fileNew,"Create a new file")
        self.actionSettings(self.ui.action_Open,"fileopen",QKeySequence.Open,self.fileOpen,"Open a preexisiting file")
        self.actionSettings(self.ui.action_Save,"filesave",QKeySequence.Save,self.fileSave,"Save current changes")
        self.actionSettings(self.ui.actionSave_as,"filesaveas",QKeySequence.SaveAs,self.fileSaveAs,"Save as a new file")
        self.actionSettings(self.ui.action_Quit,'filequit',QKeySequence.Quit,self.closeEvent,"Close the application")

        #Edit actions
        self.actionSettings(self.ui.action_add,'editadd',"Ctrl+A",self.editAdd,"add a new movie to list")
        self.actionSettings(self.ui.action_Edit,'editedit',"Ctrl+e",self.editEdit,"Edit an existing movie entry")
        self.actionSettings(self.ui.action_Delete,"editdelete", QKeySequence.Delete,self.editDelete,"Delete an existing entry")

        #Setting actions
        self.actionSettings(self.ui.action_Black,'','',lambda: self.toggleTheme('Black'),"Set Black theme")
        self.actionSettings(self.ui.action_White,'','', lambda: self.toggleTheme("White"),"Set White theme")

        #About actions
        self.actionSettings(self.ui.action_My_Movies_Help,'','',self.appHelp,"Shows help about the application")
        self.actionSettings(self.ui.action_About,'','',self.showAbout,"About the developer")

                            

    def actionSettings(self,action,icon=None,shortcut=None,slot=None,tip=None):
        if icon is not None:
            action.setIcon(QIcon(":/{}.png".format(icon)))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if slot is not None:
            action.triggered.connect(slot)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)

    def updateTable(self, current=None):

        #clearing the table and then setting its properties
        self.ui.tableWidget.clear()
        self.ui.tableWidget.setRowCount(len(self.movies))
        self.ui.tableWidget.setColumnCount(7)
        self.ui.tableWidget.setHorizontalHeaderLabels(['Title','Year','Mins','Acquired','Director','Starring','Notes'])
        self.ui.tableWidget.setAlternatingRowColors(True)
        self.ui.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
        self.ui.tableWidget.setSelectionBehavior(QTableWidget.SelectRows)
        self.ui.tableWidget.setSelectionMode(QTableWidget.SingleSelection)
        selected=None

        #populating the table
        for row, movie in enumerate(self.movies):
            item=QTableWidgetItem(movie.title)
            if current is not None  and current==id(movie):
                selected=item
            item.setData(Qt.UserRole, QVariant(id(movie)))
            self.ui.tableWidget.setItem(row,0,item)
            year=movie.year
            if year !=movie.UNKNOWNYEAR:
                item=QTableWidgetItem("{}".format(year))
                item.setTextAlignment(Qt.AlignCenter)
                self.ui.tableWidget.setItem(row,1,item)
            minutes=movie.minutes
            if minutes != movie.UNKNOWNMINUTES:
                item=QTableWidgetItem("{}".format(minutes))
                item.setTextAlignment(Qt.AlignCenter|Qt.AlignVCenter)
                self.ui.tableWidget.setItem(row,2,item)

            item=QTableWidgetItem(movie.acquired.toString("dd/MM/yyyy"))
            item.setTextAlignment(Qt.AlignCenter | Qt.AlignVCenter)
            self.ui.tableWidget.setItem(row,3,item)

            item=QTableWidgetItem(movie.director)
            item.setTextAlignment(Qt.AlignCenter | Qt.AlignVCenter)
            self.ui.tableWidget.setItem(row,4,item)

            item=QTableWidgetItem(movie.starring)
            item.setTextAlignment(Qt.AlignCenter | Qt.AlignVCenter)
            self.ui.tableWidget.setItem(row,5,item)

            notes=movie.notes
            if len(notes)>40:
                notes=notes[:40]+"..."
            self.ui.tableWidget.setItem(row,6,QTableWidgetItem(notes))

        self.ui.tableWidget.resizeColumnsToContents()
        if selected is not None :
            selected.setSelected(True)
            self.ui.tableWidget.setCurrentItem(selected)
            self.ui.tableWidget.scrollToItem(selected)

    def shouldContinue(self):
        if self.movies.isUnsaved():
            reply= QMessageBox.question(self,"unsaved Chages",
                    "Save unsaved changes?",
                    QMessageBox.Yes|QMessageBox.No|QMessageBox.Cancel)
            if reply==QMessageBox.Cancel:
                return False
            elif reply==QMessageBox.Yes:
                return self.fileSave()
        return True
            

    def fileNew(self):
        if not self.shouldContinue():
            return
        self.movies.clear()
        self.statusBar().clearMessage()
        self.updateTable()

    def fileOpen(self):
        if not self.shouldContinue():
            return
        path=QFileInfo(self.movies.fileName()).path() \
                if  self.movies.fileName() else "."

        fname=QFileDialog.getOpenFileName(self,"My Movies - Load Movie Data",path,"My Movies data files ({})".format(self.movies.formats()))
        fname=fname[0]

        if  fname:
            ok, msg=self.movies.load(fname)
            self.statusBar().showMessage(msg,5000)
            self.updateTable()


    def fileSave(self):
        filename=self.movies.fileName()
        if filename and (filename.endswith('.mqb') or filename.endswith('.mpb')):
            #print("Filesave run!")
            ok,msg=self.movies.save(self.movies.fileName())
            self.statusBar().showMessage(msg,5000)
        else:
            self.fileSaveAs()


    def fileSaveAs(self):
        file_=self.movies.fileName() if self.movies.fileName() else '.'
        file_=QFileDialog.getSaveFileName(self,
                "Save movie Data", file_,
                "My Movies data files ({})".format(self.movies.formats()))
        file_=file_[0]
        #print(file_)
        last=file_.split('/')[-1]
        if (not '.' in last) and file_:
            #print(file_)
            file_+='.mqb'
            #print(file_)
        ok,message=self.movies.save(file_)
        self.statusBar().showMessage(message,5000)

    def closeEvent(self,event):
        if self.shouldContinue():
            settings=QSettings()
            settings.setValue("LastFile",self.movies.fileName())
            settings.setValue("MainWindow/Geometry",self.saveGeometry())
            settings.setValue("MainWindow/State",self.saveState())
            settings.setValue("MainWindow/Theme",self.theme)
            self.close()
        else:
            event.ignore()

    #Edit methods
    def editAdd(self):
        dialog=addeditmoviedlg.AddEditMovieDlg(self.movies,None,self)
        if dialog.exec_():
            self.updateTable(id(dialog.movie))

    def editEdit(self):
        movie=self.currentMovie()
        if movie:
            dialog=addeditmoviedlg.AddEditMovieDlg(self.movies,movie,self)
            if dialog.exec_():
                self.updateTable(id(dialog.movie))

    def editDelete(self):
        movie=self.currentMovie()
        if movie:
            year=movie.year if movie.year!=movie.UNKNOWNYEAR else ""
            reply=QMessageBox.question(self,"My Movies - Delete movie",
                        "Do you want to delete the movie - {}({})".format(movie.title,movie.year),
                        QMessageBox.Yes|QMessageBox.No|QMessageBox.Cancel)
            if reply==QMessageBox.Yes:
                self.movies.delete(movie)
                self.updateTable()

    def currentMovie(self):
        row=self.ui.tableWidget.currentRow()
        if row>-1:
            item=self.ui.tableWidget.item(row,0)
            id_=int(item.data(Qt.UserRole))
            return self.movies.movieFromId(id_)

    #Settings methods
    def toggleTheme(self,theme):
        app=QApplication.instance()
        status=self.statusBar()
        if app is None:
            raise RuntimeError("No Application exists")
        if theme=="Black":
            self.theme='Black'
            app.setStyleSheet(styleSheetBlack)
            status.showMessage("{} theme set".format(self.theme),4500)
        elif theme=="White":
            self.theme='White'
            app.setStyleSheet(styleSheetWhite)
            status.showMessage("{} theme set".format(self.theme),4500)
        else:
            raise RuntimeError("Wrong theme?!?")

        
        
    #About methods
    def showAbout(self):
        QMessageBox.about(self,"My Movies"," This app is based on Mark Summerfield's book Rapid GUI programming in Python.\nThe developer is Your's truly, Basu Hela.")

    def appHelp(self):
        QMessageBox.about(self,"My Movies -Help","Will be added before the heat death of the universe...")

    #Initial Loading
    def loadInitialFiles(self):
        setting=QSettings()
        filename=setting.value("LastFile")
        if filename and QFile.exists(filename):
            ok,message=self.movies.load(filename)
            self.statusBar().showMessage(message,5000)
            self.updateTable()






def main():
    app=QApplication(sys.argv)
    app.setWindowIcon(QIcon(":/icon.png"))
    app.setApplicationName("My Movies")
    app.setApplicationVersion("1.2.0")
    windows=MyMovies()
    windows.show()
    sys.exit(app.exec_())

if __name__=='__main__':
    main()
