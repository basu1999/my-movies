#!/usr/bin/python3

#Imports
#------
import bisect #for maintaining the order of movie data container
import codecs #for reading python text files in different encoding format
import copyreg #related to cPickle
import pickle #for saving and loading Python "pickles"
import gzip #for compressing data
from PyQt5.QtCore import *
#---------

#Constants
#---------
CODEC="UTF-8"
NEWPARA="\u2029"
NEWLINE="\u2028"
#-----------


class Movie:
    UNKNOWNYEAR=9999
    UNKNOWNMINUTES=0

    def __init__(self,title=None,year=UNKNOWNYEAR,
                minutes=UNKNOWNMINUTES, acquired=None,director=None,starring=None, notes=None):
        self.title=title
        self.year=year
        self.minutes=minutes
        self.acquired= acquired if acquired is not None else QDate.currentDate()
        self.director=director
        self.starring=starring
        self.notes=notes

class MovieContainer:
    MAGIC_NUMBER= 0x23869E
    FILE_VERSION=169

    def __init__(self):
        self.__file=''
        self.__movies=[]
        self.__movieId={}
        self.__unsaved=False

    def __len__(self):
        return len(self.__movies)

    def __iter__(self):
        for pair in self.__movies:
            yield  pair[1]

    def isUnsaved(self):
       return self.__unsaved 

    def fileName(self):
        return self.__file

    def clear(self, clearFile=True):
        self.__movies=[]
        self.__movieId={}
        if clearFile:
            self.__file=''
        self.__unsaved=False

    def add(self,movie):
        if id(movie) not in self.__movieId:
            key=self.calculateKey(movie.title,movie.year)
            bisect.insort_left(self.__movies,[key,movie])
            self.__movieId[id(movie)]=movie

            self.__unsaved=True
            return True

    def calculateKey(self,title,year):
        text=title.lower()
        if text.startswith("a "):
            text=text[2:]
        elif text.startswith("an "):
            text=text[3:]
        elif text.startswith("the "):
            text=text[4:]

        #padding the key with zeros if  title begin with number so that 
        #, for example, "20" come before "100".
        parts=text.split(" ",1)
        if parts[0].isdigit():
            text="{:08}".format(int(parts[0]))
            if len(parts)>1:
                text+=parts[1]
        return "{}\t{}".format(text.replace(" ",""),year)

    def delete(self,movie):
        if id(movie) in self.__movieId:
            key=self.calculateKey(movie.title,movie.year)
            index=bisect.bisect_left(self.__movies,[key,movie])
            del self.__movies[index]
            del self.__movieId[id(movie)]
            self.__unsaved=True
            return True

    def updateMovie(self,movie, title, year, minutes=None, director=None,starring=None,notes=None):
        if  minutes :
            movie.minutes=minutes
        if notes:
            movie.notes=notes
        if title !=movie.title or year !=movie.year:
            key=self.calculateKey(movie.title,movie.year)
            i=bisect.bisect_left(self.__movies,[key,movies])
            self.__movies[i][0]=self.calculateKey(title,year)
            movie.title=title
            movie.year=year
            self.__movies.sort()
        if director:
            movie.director=director
        if starring:
            movie.starring=starring
        self.__unsaved=True

    
    @staticmethod
    def formats():
        return "*.mqb "

    def movieFromId(self,id_):
        return self.__movieId[id_]

    def save(self,file_=''):
        """This method will pass down the work of saving to another method
        based on the extension of the file_"""
        self.__file=file_
        if self.__file.endswith('.mqb'):
            return self.saveQStream()
        else:
            return False,"Invalid file extenstion, failed to save!"

    def load(self,file_=''):
        """This method will pass down the work of loading to another method
        based on the extesion of the file_"""
        self.__file=file_
        if self.__file.endswith(".mqb"):
            return self.loadQStream()
        else:
            return False,"Invalid file extension, failed to load!"

    def saveQStream(self):
        fh=None
        try:
            fh=QFile(self.__file)
            if not fh.open(QIODevice.WriteOnly):
                raise IOError(fh.errorString())

            oStream=QDataStream(fh)
            oStream.writeInt64(MovieContainer.MAGIC_NUMBER)
            oStream.writeInt64(MovieContainer.FILE_VERSION)
            oStream.setVersion(QDataStream.Qt_5_9) #When loading data, the same version must be used

            #Writing movies data to file
            for key, movie in self.__movies:
                oStream.writeQString(movie.title)
                oStream.writeInt32(movie.year)
                oStream.writeInt32(movie.minutes)
                oStream<<movie.acquired
                oStream.writeQString(movie.director)
                oStream.writeQString(movie.starring)
                oStream.writeQString(movie.notes)

        except (IOError, OSError) as err:
            error="Failed to save: {}".format(err)
            if fh is not None:
                fh.close()
            return False, error
        else:
            fh.close()
            self.__unsaved=False
            return True, "Saved {} movies to {}".format(len(self.__movies),
                                            self.__file)

    def loadQStream(self):
        fh=None
        try:
            fh=QFile(self.__file)
            if not fh.open(QIODevice.ReadOnly):
                raise IOError(fh.errorString())
            IStream=QDataStream(fh)
            magic_num=IStream.readInt64()
            if magic_num !=MovieContainer.MAGIC_NUMBER:
                raise IOError("Unrecognized file type")
            version=IStream.readInt64()
            if version<MovieContainer.FILE_VERSION:
                raise IOError("Old and unreadable file format")
            if version>MovieContainer.FILE_VERSION:
                raise IOError("new and unreadblae file format")
            self.clear(False)
            IStream.setVersion(QDataStream.Qt_5_9)
            
            while not IStream.atEnd():
                acquired=QDate()
                title=IStream.readQString()
                year=IStream.readInt32()
                minutes=IStream.readInt32()
                IStream>>acquired
                director=IStream.readQString()
                starring=IStream.readQString()
                notes=IStream.readQString()
                self.add(Movie(title,year, minutes, acquired,director,starring, notes))
        except (IOError, OSError) as err:
            error= "Failed to load: {}".format(err)
            if fh is not None:
                fh.close()
            return False, error
        else:
            fh.close()
            self.__unsaved=False
            return True, "Loaded {} movies from {}".format(len(self.__movies),self.__file)

    #Writing and Reading Using the pickle Module
    def _pickleQDate(date):
        return QDate,(date.day(),date.month(),date.year())

    copyreg.pickle(QDate,_pickleQDate)

    def savePyStream(self):
        fh=None
        try:
            fh=gzip.open(self.__file,'wb')
            pickle.dump(self.__Movies,fh,2) #2 means pickle binary format
        except (IOError, OSError) as err:
            error="Failed to save: {}".format(err)
            if fh is not None:
                fh.close()
            return False, error
        else:
            fh.close()
            self.__unsaved=False
            return True, "Saved {} movie records to {}".format(len(self.__movies),self.__file)

    def loadPyStream(self):
        fh=None
        try:
            fh=gzip.open(self.__file,'rb')
            self.clear(False)
            self.__movies=pickle.load(fh)
            for key,movie in self.__movies:
                self.__movieId[key]=movie
        except (IOError,OSError) as err:
            error="Failed to load: {}".format(err)
            if fh is not None:
                fh.close()
            return False, error
        else:
            fh.close()
            self.__unsaved=False
            return True,"Loaded {} movie records from {}".format(len(self.__movies),self.__file)






    





        



            



