#!/usr/bin/python3

#Imports
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import sys

#Custom Imports
from ui_addeditDialog import *
import moviedata

class AddEditMovieDlg(QDialog):
    def __init__(self,movieContainer,movie=None,Parent=None):
        super().__init__(Parent)
        self.ui=Ui_Dialog()
        self.ui.setupUi(self)
        self.movies=movieContainer
        self.movie=movie
        self.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)

        if self.movie is None:
            self.setWindowTitle("My Movies - Add")
            today=QDate.currentDate()
            year=QDate.year(today)
            self.ui.spinBox_Year.setValue(year)
            self.ui.dateEditAcquired.setDate(today)
            self.ui.lineEdit_movieName.setFocus()

        else :
            self.setWindowTitle("My Movies - Edit")
            self.ui.lineEdit_movieName.setText(movie.title)
            self.ui.lineEdit_Notes.setText(movie.notes)
            self.ui.lineEdit_Director.setText(movie.director)
            self.ui.lineEdit_Starring.setText(movie.starring)
            self.ui.spinBox_Year.setValue(movie.year)
            self.ui.spinBox_Minutes.setValue(movie.minutes)
            self.ui.dateEditAcquired.setDate(movie.acquired)

        self.show()

        #Connections
        self.ui.pushButton_Save.clicked.connect(self.accepted)
        

    def accepted(self):
        #print("working")
        title=self.ui.lineEdit_movieName.text()
        year=self.ui.spinBox_Year.value()
        minutes=self.ui.spinBox_Minutes.value()
        acquired=self.ui.dateEditAcquired.date()
        notes=self.ui.lineEdit_Notes.text()
        director=self.ui.lineEdit_Director.text()
        starring=self.ui.lineEdit_Starring.text()
        #print("Title={}\nYear={}\nAcquired={}\nminutes={}\nNotes={}".format(title,year,acquired,minutes,notes))

    
        if self.movie is None:
            #Create a new movie object
            self.movie=moviedata.Movie(title,year,minutes,acquired,director,starring,notes)
        else:
            #Edit the existing movie object
            self.movie.title=title
            self.movie.year=year
            self.movie.minutes=minutes
            self.movie.acquired=acquired
            self.movie.starring=starring
            self.movie.director=director
            self.movie.notes=notes

        #Adding the movie object to the movie container
        self.movies.add(self.movie)
        
        #returning
        self.accept()



        
    







if __name__=='__main__':
    con=moviedata.MovieContainer()
    date=QDate.fromString('31122019','dMyyyy')
    mov=moviedata.Movie("Endgame",2019,120,date,"I am Basu")
    app=QApplication(sys.argv)
    dialog=AddEditMovieDlg(con,mov)
    dialog.show()
    sys.exit(app.exec_())



    

